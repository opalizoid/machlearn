# Instead of :input, path to input file

text = File.read(:input)

data_array = Array.new

text.each_line.with_index do |line, index|
  data_array[index] = line.split(' ')
end

=begin
data_array.each_index do |i|
  data_array[i].each_index do |j|
    data_array[i][j] = data_array[i][j].to_i
  end
end
=end

tmp = data_array.transpose

hash = { }

# Instead of :title, title of column
# Instead of :index, index of dataset column (from zero)

hash[:title] = tmp[:index]

# Instead of :file, path to output file

File.open(:file, 'wb') {|f| f.write(Marshal.dump(hash))}
