repair = Marshal.load(File.binread(File.join('../include/housing.data.binrb')))
result = { }

repair.each do |k, v|
  arr = Array.new
  v.each do |v_l|
    arr << v_l.to_f
  end
  result[k] = arr
end

File.open('housing.data.binrb', 'wb') {|f| f.write(Marshal.dump(result))}
