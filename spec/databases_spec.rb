require 'main'

describe 'Databases module' do
  context 'creates database of' do
    it "boston dataset" do
      hash = Databases.load_boston
      keys = hash.keys.size
      values_first = hash.values.first.size
      values_last = hash.values.last.size
      expect(keys).to eq 14
      expect(values_first).to eq values_last
    end
  end
end
