module Databases
  INCLUDE = File.expand_path('./include')
  
  def Databases.load_boston
    Marshal.load(File.binread(File.join(INCLUDE, './housing.data.binrb')))
  end
  
  def Databases.load_custom
    Marshal.load(File.binread(File.join(INCLUDE, './custom.data.binrb')))
  end
end
