module Regression
  module RegressionUnvariable
    def RegressionUnvariable.squared_error(x, y, theta_zero, theta_one)
      ((theta_zero + theta_one * x) - y)**2
    end
    def RegressionUnvariable.theta_zero_equation(x, y, theta_zero, theta_one)
      ((theta_zero + theta_one * x) - y)
    end
    def RegressionUnvariable.theta_one_equation(x, y, theta_zero, theta_one)
      ((theta_zero + theta_one * x) - y) * x
    end
  end
  module RegressionMultivariate
    def RegressionMultivariate.theta_vector_equation(x_vector, theta_vector, y, x)
      (theta_vector.inner_product(x_vector) - y) * x
    end
  end
end
