require 'modules/databases'
require 'modules/regression'
require 'modules/classification'

require 'classes/class_regression'

class Main < RegressionClass
  class << self    
    def call(db)
      if db == :boston
        @database = Databases.load_boston
      elsif db == :iris
        nil
      elsif db == :custom
        @database = Databases.load_custom
      end
      self
    end
  
    def call_database(columns)
      tmp = Array.new
      columns.each do |index|
        tmp << @database.values[index]
      end
      @dataset = tmp
      self
    end
    
    def call_split_x_y
      @dataset = [@dataset[0...@dataset.size-1], @dataset.last]
      self
    end
    
    def call_normalization
      average = Array.new
      range = Array.new
      @dataset.first.each_index do |index|
        average << @dataset.first[index].inject { |sum, x| sum + x } / @dataset.first[index].size.to_f
        min, max = @dataset.first[index].minmax
        range << max - min
      end
      @dataset.first.each_index do |i_index|
        @dataset.first[i_index].each_index do |j_index|
          @dataset.first[i_index][j_index] = (@dataset.first[i_index][j_index] - average[i_index]) / range[i_index]
        end
      end
      self
    end
  
    def call_regression(alpha)
      if @dataset.first.size > 1
        result = do_regression_multivariate(@dataset, alpha)
      elsif @dataset.first.size == 1
        result = do_regression_unvariable(@dataset, alpha)
      end
      p @dataset.last[1]
      p result[0] + result[1] * @dataset.first[1][1] + @dataset.first[1][2] * result[2]
      p [@dataset.first[1][1], @dataset.first[1][2]]
      self
    end
    
    def call_classification
      if @dataset.first.size < 1
        nil
      elsif @dataset.first.size == 1
        Classification::ClassificationUnvariable.cost_function(@dataset)
      else
        nil
      end
    end
  end
end

class Bin
  def self.exe
    main = Main.call(:boston)
    main.call_database([5, 6, 11])
    main.call_split_x_y
    main.call_normalization
    main.call_regression(0.01)
  end
end
