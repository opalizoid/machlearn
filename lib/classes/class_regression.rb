require 'matrix'

class RegressionClass
  def self.do_regression_unvariable(dataset, alpha)
    delta = 1
    theta_zero = 0
    theta_one = 0
    lambda_cost_function = ->(i, t_o, t_t) { Regression::RegressionUnvariable.squared_error(dataset.first.first[i], dataset.last[i], t_o, t_t) }
    lambda_gradient_descend_zero = ->(i, t_o, t_t) { Regression::RegressionUnvariable.theta_zero_equation(dataset.first.first[i], dataset.last[i], t_o, t_t) }
    lambda_gradient_descend_one = ->(i, t_o, t_t) { Regression::RegressionUnvariable.theta_one_equation(dataset.first.first[i], dataset.last[i], t_o, t_t) }
    while delta > 0.001
      cost_one = 0
      dataset.first.first.each_index do |index|
        cost_one += lambda_cost_function.call(index, theta_zero, theta_one)
      end
      cost_one = (1.0 / (2.0 * dataset.first.first.size.to_f)) * cost_one
      sum_zero = 0
      dataset.first.first.each_index do |index|
        sum_zero += lambda_gradient_descend_zero.call(index, theta_zero, theta_one)
      end
      sum_one = 0
      dataset.first.first.each_index do |index|
        sum_one += lambda_gradient_descend_one.call(index, theta_zero, theta_one)
      end
      theta_zero = theta_zero - alpha * (1.0 / dataset.first.first.size.to_f) * sum_zero
      theta_one = theta_one - alpha * (1.0 / dataset.first.first.size.to_f) * sum_one
      cost_two = 0
      dataset.first.first.each_index do |index|
        cost_two += lambda_cost_function.call(index, theta_zero, theta_one)
      end
      cost_two = (1.0 / (2.0 * dataset.first.first.size.to_f)) * cost_two
      delta = cost_one - cost_two
    end
    [theta_zero, theta_one]
  end
  
  def self.do_regression_multivariate(dataset, alpha)
    dataset.first.insert(0, Array.new(dataset.first.first.size).fill(1.0))
    y = dataset.last
    dataset = Matrix.rows(dataset.first, copy=true).transpose
    row_size = dataset.row_count
    column_size = dataset.column_size
    theta_vector = Vector.elements(Array.new(column_size).fill(0.0), copy=true)
    lambda_gradient_descend_vector = ->(x_vec, t_vec, y, x) { Regression::RegressionMultivariate.theta_vector_equation(x_vec, t_vec, y, x) }
    i = 0
    while i < 1000
      sum_vector = Array.new
      column_size.times do |i_index|
        sum = 0
        row_size.times do |j_index|
          sum += lambda_gradient_descend_vector.call(dataset.row(j_index), theta_vector, y[j_index], dataset.element(j_index, i_index))
        end
        sum_vector << sum
      end
      theta_vector = theta_vector.to_a
      sum_vector.each_index do |index|
        theta_vector[index] = theta_vector[index] - alpha * (1.0 / row_size) * sum_vector[index]
      end
      theta_vector = Vector.elements(theta_vector, copy=true)
      i += 1
    end
    theta_vector.to_a
  end
end
